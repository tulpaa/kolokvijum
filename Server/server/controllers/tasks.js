const { json } = require("express");
const mongoose = require("mongoose");
const taskModel = require("../models/tasks");

const getAllTasks = async (req, res, next) => {
    try{
        const allTasks = await taskModel.find({}).exec();

        res.status(200).json(allTasks);
    }
    catch(err){
        next(err);
    }
}

const postNewTask = async (req, res, next) => {
    const {title,description,priority,status} = req.body;
    
    try{
        const newTask = new taskModel({
            _id : new mongoose.Types.ObjectId(),
            title : title,
            description : description,
            priority : priority,
            status : status,
        });

        const newTaskResponse = await newTask.save();
        console.log(newTaskResponse);

        res.status(200).json(newTaskResponse);

    }
    catch(err){
        next(err);
    }
}

const getTaskById = async (req, res, next) => {
    const taskId = req.params.id;

    try{ 
        const taskById = await taskModel.findById(taskId);

        res.status(200).json(taskById);
    }
    catch(err){
        next(err);
    }
}

module.exports = { 
    getAllTasks,
    postNewTask,
    getTaskById,
}