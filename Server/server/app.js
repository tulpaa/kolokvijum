// Konfigurisanje express servera 
const express = require('express');
const { urlencoded, json } = require('body-parser');
const mongoose = require('mongoose');

const app = express();

// Povezivanje sa bazom podataka
const databaseString = 'mongodb://localhost:27017/kolokvijum';
mongoose.connect(databaseString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Kada se povezemo prvi put dobijamo poruku
mongoose.connection.once('open', function () {
  console.log('Uspesno povezivanje!');
});

// Ako se dogodi greska pri povezivanju
mongoose.connection.on('error', (error) => {
  console.log('Greska: ', error);
});

// Samo deklarisemo da koristimo json i urlencoded
app.use(json());
app.use(urlencoded({ extended: false }));

// Postavljamo header-e servera koje mozemo dobiti ako ga pingujemo npr
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  if (req.method === 'OPTIONS') {
    res.header(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PUT, PATCH, DELETE'
    );

    return res.status(200).json({});
  }

  next();
});

// Povezivanje sa rutama 
const taskRoutes = require('./routes/tasks');
// Ovo /api/tasks predstavlja kako pozivamo url-om 
// pr: http://localhost:3000/api/tasks/
// Ako stavimo samo tasks onda http://localhost:3000/tasks
app.use('/tasks', taskRoutes);


// Obrada greske u slucaju pogresnog zahteva
app.use(function (req, res, next) {
  const error = new Error('Zahtev nije podrzan!');
  error.status = 405;

  next(error);
});

app.use(function (error, req, res, next) {
  const statusCode = error.status || 500;
  res.status(statusCode).json({
    error: {
      message: error.message,
      status: statusCode,
      stack: error.stack,
    },
  });
});

module.exports = app;
