const mongoose = require("mongoose");

const tasksSchema = mongoose.Schema({

    _id : mongoose.Schema.Types.ObjectId,

    title : {
        type : String,
        required : true,
    },

    description : { 
        type : String,
        required : true,
    },

    priority : {
        type : String,
        required : true,
    },

    status : {
        type : String,
        required : true,
    }
});

const tasksModel = mongoose.model("tasks",tasksSchema);

module.exports = tasksModel;
