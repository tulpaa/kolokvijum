const express = require('express');
const { route } = require('../app');
const router = express.Router();

const taskController = require("../controllers/tasks");

router.get("/",taskController.getAllTasks);
router.get("/:id",taskController.getTaskById);
router.post("/",taskController.postNewTask);


module.exports = router;